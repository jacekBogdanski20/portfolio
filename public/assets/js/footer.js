"use strict";
const contact = document.querySelector(".hero__contact--js")
const email = document.querySelector(".contact__email--js")
const skills = document.querySelector(".skills--js")
const projects = document.querySelector(".projects--js")

const headers = new Headers({
    'Content-Type': 'text/plain',
    'Access-Control-Allow-Origin': "https://api.bitbucket.org"
});
const request = new Request({
    method: 'POST',
    mode: 'cors',
    headers: headers
});
fetch("assets/data/data.json")
    .then(resp => resp.json())
    .then(resp => {
        contact.innerHTML = `<a href="mailto:${resp.contact.email}">${resp.contact.email}</a>`;
        email.innerHTML = `<a href="mailto:${resp.contact.email}">${resp.contact.email}</a>`;

        let skill;
        let skillList = '';
        for (let i in resp.skills) {

            skill = resp.skills[i];

            skillList += `<article class="skill skill--${i}">\
            <hr class="skill__line skill__line--${i}">
            <h2 class="skill__title">${skill.title}</h2>
            </article>`;

            skillList += `<article class="skill skill--${i}">\
                <ul class="skill__list">`;
            for (let li of skill.values) {
                skillList += `<li>${li}</li>`;
            }
            skillList += `</ul></article>`
        }
        skills.innerHTML += skillList;
    })
    .catch(error => {
        console.log(error)
    })

fetch("https://api.bitbucket.org/2.0/repositories/jacekBogdanski20/", request)
    .then(resp => resp.json())
    .then(resp => {
        const repos = resp.values;
        console.log(resp.values);
        length = repos.length;

        for (let i = length - 1; i > length - 5; i--) {
            if (repos[i] == null) break;
            if (repos[i].website == null) repos[i].website = "#";
            projects.innerHTML += `<article class="project">
                <img class="project__logo" src="https://c616b3614506f07e89ab-5cef763fe73fcec814d61b3b60a1ac9a.ssl.cf1.rackcdn.com/V1~13312fab-0de6-4f94-bea2-67df374a56cd~89a916c5b29f49cf9cc56cd24db5719a" height="50px" width="50px;">
                <h2 class="project__name">
                    ${repos[i].name}
                </h2>
                <p class="project__description">
                    ${repos[i].description}
                </p>
                <p class="project__links">
                    <a class="project__link project__link--demo" href="${repos[i].website}">Show</a> | 
                    <a class="project__link project__link--repo" href="${repos[i].links.html.href}">Bitbucket</a>
                </p>
            </article>`;
        }
    })
    .catch(error => {
        console.log(error)
    })