'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var csso = require('gulp-csso');

var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

gulp.task('scss-first', function() {
    return gulp.src('./public/assets/scss/style-default-first.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
        .pipe(csso())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/assets/css/'));
});

gulp.task('scss-last', function() {
    return gulp.src('./public/assets/scss/style-default-last.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
        .pipe(csso())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/assets/css/'));
});

gulp.task('watch', function() {
    gulp.watch('./public/assets/scss/**/*.scss', gulp.series('scss-first'));
    gulp.watch('./public/assets/scss/**/*.scss', gulp.series('scss-last'));
});